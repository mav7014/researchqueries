<<<<<<<<<<<<<<<  CLABSI  >>>>>>>>>>>>>>>>
-- Create Dataset as a subset of PERSON
/*This query selects all variables from the person table.
The rows are restricted to the patients whose person_source_value is equal to one of the
Site Proxy ID's in the CLABSI Patient List.
Additional variables are:
1. a flag for the infected cohort, called "which_cohort" below, 
2. Site name, fill in your own site name, 
3. the CDRN_ID from the clabsi_patient_list, called "subject_id" below,
4. the MRN obtained from a site-specific source, called "mrn" below, joined according to 
site-specific rules.
 
It is assumed that every patient in the list has an MRN. */
 
SELECT 1 AS "which_cohort" 
,‘your site name’ AS site 
,a.*
,b.CDRN_ID AS "subject_id" 
,c.{MRN from your site-specific source} AS "mrn"
,c.{First Name from your site-specific source} AS "first_name"
,c.{Last Name from your site-specific source} AS "last_name"
FROM person a 
,cauti_patient_list b 
,{local source for MRN}  c
where a.person_source_value = b.Site_proxy_id
and (add your own join criteria for MRN) 
;
 
-- Create Dataset as a subset of VISIT_OCCURRENCE
/*This query selects all variables from the visit_occurrence table.
The rows are restricted to 
1. the patients whose person_source_value is equal to one of the Site Proxy ID's in the 
CLABSI Patient List, 
2. whose visits occur within the date range of '1-Oct-2010' to '30-Sep-2015'. 
 
It is assumed that the person_source_value is in the visit_occurrence table (it is not in mine),
and that it is the same as the Site_proxy_id in the clabsi_patient_list.*/
SELECT a.* 
FROM visit_occurrence a 
,clabsi_patient_list b 
where a.person_source_value = b.Site_proxy_id 
AND a.visit_start_date BETWEEN '1-Oct-2010' AND '30-Sep-2015'
;
 
-- Create Dataset as a subset of CONDITION_OCCURRENCE
/*This query selects all variables from the condition_occurrence table.
The rows are restricted to:
1. the patients whose person_source_value is equal to one of the Site Proxy ID's in the 
CLABSI Patient List, 
2. visits with condition_source_concept_id equal to one of the eleven concept ID's that identify clabsi 
infection,
3. visits that occur within the date range of '1-Oct-2010' to '30-Sep-2015'. 
 
It is assumed that the person_source_value in the visit_occurrence table (it is not in mine),
and that it is the same as the Site_proxy_id in the clabsi_patient_list.*/
SELECT a.* 
FROM condition_occurrence a 
,visit_occurrence b 
,clabsi_patient_list c 
where a.visit_occurrence_id = b.visit_occurrence_id
and b.person_source_value = c.Site_proxy_id
AND a.condition_source_concept_id IN (44823651,44832931,
                44829481,44823653,44820255,44826030,44826040,
                44820260,44830639,44828362,44826041)
and b.visit_start_date BETWEEN '1-Oct-2010' AND '30-Sep-2015' 
;
