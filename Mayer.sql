General Instructions

-- We based this on instructions Jacob had developed for the Grinspan study.  All feedback welcome as we would like this to become a standard template

-- We have left wcmc as the default (please adjust for your site)

-- For the data retrieval step, based on feedback from one site, we have included the actual field names in the select clause. 


-- STEP 1: Prepare new / update existing mapping table for CDRN projects

CREATE TABLE [dbo].[PERSON_MEMBER_MAPPING](
[person_source_value] varchar(100) NOT NULL,
[mayer_sssid] varchar(100) NULL --add this column for mayer study
) ON [PRIMARY]

-- STEP 2: Use own code to update table to include all your site's existing person_source_values 

-- NOTE: If you are not already, please start preserving all your mappings between source values and SSSIDs, perhaps in the same table, since some of these projects have multiple phases; or might need adjustments over time

-- STEP 3: Sites run update statement below to incorporate SSSID into the mapping table for the Mayer study
 
UPDATE person_member_mapping
SET mayer_sssid = nygc.sssid
FROM WCMC_MapSet_Mayer nygc where 'WCMC:'+cast(person_source_value as varchar) = nygc.ehrId --substitute "wcmc" with your site code

-- STEP 4: Data retrieval. Sites will run queries below and generate separate files(CSV format) for each result set. 

Person
select pmm.mayer_sssid as sssId,
p.datetime_of_birth,p.gender_concept_id,p.race_concept_id,p.ethnicity_concept_id
from PERSON p
inner join person_member_mapping pmm on p.person_source_value = pmm.person_source_value
where mayer_sssid is not null

Visit_Occurrence
select pmm.mayer_sssid as sssId,
vo.visit_occurrence_id,vo.visit_concept_id,vo.visit_start_date,vo.visit_end_date,vo.visit_start_time,vo.visit_end_time
from VISIT_OCCURRENCE vo
inner join person_member_mapping pmm on vo.person_source_value = pmm.person_source_value
where mayer_sssid is not null
and visit_start_date < '01/01/2015'

Observation (observations related to Visit_Occurrence records not to Diagnosis records / or all observations if easier)
select pmm.mayer_sssid as sssId,
o.observation_id,o.observation_concept_id,o.observation_date,o.value_as_concept_id,o.visit_occurrence_id,o.observation_concept_id
from OBSERVATION o
inner join person_member_mapping pmm on o.person_source_value = pmm.person_source_value
where mayer_sssid is not null
and observation_date < '01/01/2015'

Condition_Occurrence
select pmm.mayer_sssid as sssId,
co.condition_occurrence_id,co.condition_concept_id,co.condition_start_date,co.condition_type_concept_id,co.visit_occurrence_id,co.condition_source_value,co.condition_source_concept_id
from CONDITION_OCCURRENCE co
inner join person_member_mapping pmm on co.person_source_value = pmm.person_source_value
where mayer_sssid is not null
and condition_start_date < '01/01/2015'

Procedure_Occurrence
select pmm.mayer_sssid as sssId,
po.procedure_occurrence_id,po.procedure_concept_id,po.procedure_date,po.visit_occurrence_id,po.procedure_source_value,po.procedure_source_concept_id
from PROCEDURE_OCCURRENCE po
inner join person_member_mapping pmm on po.person_source_value = pmm.person_source_value
where mayer_sssid is not null
and procedure_date < '01/01/2015'

Measurement
select pmm.mayer_sssid as sssId,
m.measurement_id,m.measurement_concept_id,m.measurement_date,m.measurement_type_concept_id,m.value_as_number,m.unit_concept_id
from MEASUREMENT m
inner join person_member_mapping pmm on m.person_source_value = pmm.person_source_value
where mayer_sssid is not null
and measurement_date < '01/01/2015'

Fact_Relationship
select *
from FACT_RELATIONSHIP fr where fact_id_1 in(select measurement_id from measurement m
inner join person_member_mapping pmm on m.person_source_value = pmm.person_source_value
where mayer_sssid is not null
and measurement_date < '01/01/2015'

Drug_Exposure
select pmm.mayer_sssid as sssId,
d.drug_exposure_id,d.drug_concept_id,d.drug_exposure_start_date,d.drug_exposure_end_date,d.drug_type_concept_id,d.stop_reason,d.refills,d.quantity,d.route_concept_id,d.effective_drug_dose,d.dose_unit_concept_id,d.visit_occurrence_id,d.drug_source_concept_id
from DRUG_EXPOSURE d
inner join person_member_mapping pmm on d.person_source_value = pmm.person_source_value
where mayer_sssid is not null
and drug_exposure_start_date < '01/01/2015'


