# README #

* This repository will store query code for each NYC-CDRN research project
* Over time we will publish more specific instructions about how we will manage version control for each query
* For now, please report any issues with the code (and suggested fixes) directly to NYC-CDRN Project Ops - Alex Low and Joan Leavey.
