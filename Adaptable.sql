General Instructions

-- We based this on instructions Jacob had developed for the Grinspan study.  All feedback welcome as we would like this to become a standard template

-- We have left wcmc as the default (please adjust for your site)

-- For the data retrieval step, based on feedback from one site, we have included the actual field names in the select clause. 

-- The id fields for each table should be passed as a varchar(100).  This includes sssID, person_source_value, visit_occurrence_id, -- observation_id, procedure_occurrence_id, condition_occurrence_id, measurement_id and drug_exposure_id


-- STEP 1: Prepare new / update existing mapping table for CDRN projects

CREATE TABLE [dbo].[PERSON_MEMBER_MAPPING](
[person_source_value] varchar(100) NOT NULL,
[adaptable_sssid] varchar(100) NULL --add this column for adaptable study  
) ON [PRIMARY]

-- STEP 2: Use adaptable data file received from Central Ops to populate table. Adaptable data file will contain person_source_value (proxy_id), new SSSID, and site code.   

-- NOTE: If you are not already, please start preserving all your mappings between source values and SSSIDs, perhaps in the same table, since some of these projects have multiple phases; or might need adjustments over time

-- STEP 3: Sites run update statement below to incorporate SSSID into the mapping table for the adaptable study

UPDATE person_member_mapping
SET adaptable_sssid = nygc.sssid
FROM WCMC_MapSet_Adaptable nygc where 'WCMC:'+cast(person_source_value as varchar) = nygc.ehrId --substitute "wcmc" with your site code

-- STEP 4: Data retrieval. Sites will run queries below and generate separate files(CSV format) for each result set. Please replace the value for care_site_id with your code.  This value can be found in the 'Instructions for Adaptable Study Specific Data Mart.doc' 

Person
select pmm.adaptable_sssid as sssId,
p.year_of_birth,p.month_of_birth,p.day_of_birth,p.gender_concept_id,p.race_concept_id,p.ethnicity_concept_id, pmm.adaptable_ssid as p.person_source_value, 3 as care_site_id 
from PERSON p
inner join person_member_mapping pmm on p.person_source_value = pmm.person_source_value
where adaptable_sssid is not null

Visit_Occurrence
select pmm.adaptable_sssid as sssId,
vo.visit_occurrence_id,vo.visit_concept_id,vo.visit_start_date,vo.visit_end_date,3 as care_site_id 
from VISIT_OCCURRENCE vo
inner join person_member_mapping pmm on vo.person_source_value = pmm.person_source_value
where adaptable_sssid is not null
and visit_start_date > '01/01/2000'

Observation
select pmm.adaptable_sssid as sssId,
o.observation_id,o.observation_concept_id,o.observation_date,o.observation_type_concept_id,o.value_as_concept_id,o.visit_occurrence_id,3 as care_site_id 
from OBSERVATION o
inner join person_member_mapping pmm on o.person_source_value = pmm.person_source_value
where adaptable_sssid is not null
and observation_date > '01/01/2000'

Condition_Occurrence
select pmm.adaptable_sssid as sssId,
co.condition_occurrence_id,co.condition_concept_id,co.condition_start_date, co.condition_end_date, co.condition_type_concept_id,co.visit_occurrence_id,
co.condition_source_value,co.condition_source_concept_id,co.vocabulary_id,3 as care_site_id
from CONDITION_OCCURRENCE c
inner join person_member_mapping pmm on co.person_source_value = pmm.person_source_value
where adaptable_sssid is not null
and condition_start_date > '01/01/2000'

Procedure_Occurrence
select pmm.adaptable_sssid as sssId,
po.procedure_occurrence_id,po.procedure_concept_id,po.procedure_date,po.procedure_type_concept_id,po.visit_occurrence_id,po.procedure_source_value,
po.procedure_source_concept_id,po.vocabulary_id,3 as care_site_id 
from PROCEDURE_OCCURRENCE po
inner join person_member_mapping pmm on po.person_source_value = pmm.person_source_value
where adaptable_sssid is not null
and procedure_date > '01/01/2000'

Measurement
select pmm.adaptable_sssid as sssId,
m.measurement_id,m.measurement_concept_id,m.measurement_date,m.measurement_type_concept_id,m.operator_concept_id,
m.value_as_number, m.unit_concept_id, m.visit_occurrence_id, m.measurement_source_value, 
m.measurement_source_concept_id,3 as care_site_id
from MEASUREMENT m
inner join person_member_mapping pmm on m.person_source_value = pmm.person_source_value
where adaptable_sssid is not null
and measurement_date > '01/01/2000'

Fact_Relationship_m (measurement)
select domain_concept_id_1, fact_id_1, domain_concept_id_2, fact_id_2, relationship_concept_id,3 as care_site_id
from Fact_relationship fr where fact_id_1 in(select measurement_id from measurement m
inner join person_member_mapping pmm on m.person_source_value = pmm.person_source_value
where adaptable_sssid is not null
and measurement_date > '01/01/2000')

Fact_Relationship_co (condition_occurrence)
select domain_concept_id_1, fact_id_1, domain_concept_id_2, fact_id_2, relationship_concept_id,3 as care_site_id
from Fact_relationship fr where fact_id_1 in(select conditon_occurrence_id from condition_occurrence co
inner join person_member_mapping pmm co.person_source_value = pmm.person_source_value
where adaptable_sssid is not null
and condition_start_date > '01/01/2000')

Drug Exposure
select pmm.adaptable_sssid as sssId,
de.drug_exposure_id,de.drug_concept_id,de.drug_exposure_start_date,de.drug_exposure_end_date,de.drug_type_concept_id,
de.refills, de.quantity, de.days_supply, de.route_concept_id, de.effective_drug_dose, de.dose_unit_concept_id, de.visit_occurrence_id,
de.drug_source_concept_id,3 as care_site_id 
from DRUG_EXPOSURE de
inner join person_member_mapping pmm on po.person_source_value = pmm.person_source_value
where adaptable_sssid is not null
and drug_exposure_start_date > '01/01/2000'
